import logo from './logo.svg';
import './App.css';
import React from 'react';

const learn_react = React.createElement(
  'a',
  {
    className: "App-link",
    href: "https://reactjs.org",
    target: "_blank",
    rel: "noopener noreferrer"
  },
  'Learn React ->'
)
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        {learn_react}
      </header>
    </div>
  );
}

export default App;
